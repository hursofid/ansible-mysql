# Ansible role for MySQL installation #

## Prerequisites ##

- Ansible 2+

- Debian 10, 11 or Ubuntu (not tested) on remote

## Usage ##

Clone this repository and run against your hosts.yml:

```
ansible-playbook -i /etc/ansible/hosts.yml --limit databasehost main.yml
```

## Variables ##

#### mysql_version ####

- mysql-5.7

- mysql-8.0

#### keyserver ####

- pgp.net.nz

#### key_id ####

- 3A79BD29

## Reference ##

- https://dev.mysql.com/doc/mysql-apt-repo-quick-guide/en/index.html#repo-qg-apt-repo-manual-setup
